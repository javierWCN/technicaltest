<?php
class Sql
{
	private $connect;

	public function __construct(){
    $this->connect = new mysqli("127.0.0.1","wcn","",'prueba');
    if ($this->connect)
    {
        $this->connect->set_charset('SET NAMES "utf8"');
    }

	}
	public function __destruct(){
		//Destructor
		$this->connect->close();
	}

	public function setDB($dbName){
	   $this->connect->select_db($dbName);
  }

  public function getConnection()
  {
    return $this->connect;
  }

}
