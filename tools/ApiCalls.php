<?php
class ApiCalls
{
  public static function call($url,$method,$data=null)
  {
    $curl = curl_init();
    //$method = "GET";
    //$data = [];
    //$url = "";

    switch ($method) {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, true);
            if ($data) {
                $payload = http_build_query($data);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
            }
            break;
        case "PUT":
            $setup_headers[] = 'Content-Type: application/json';
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            if ($data) {
                $payload = json_encode($data);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
            }
            break;
        case "DELETE":
            $setup_headers[] = 'Content-Type: application/json';
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
            if ($data) {
                $payload = json_encode($data);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
            }
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLINFO_HEADER_OUT, true);
    //curl_setopt($curl, CURLOPT_HTTPHEADER,$setup_headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($curl);
    if (!$result) {
        die("Connection Failure: " . curl_error($curl));
    }
    curl_close($curl);
    return $result;

  }
}
?>
